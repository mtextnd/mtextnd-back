-- AlterTable
ALTER TABLE "MessageTemplate" ADD COLUMN     "messageTypeId" TEXT;

-- CreateTable
CREATE TABLE "AppRefreshToken" (
    "id" SERIAL NOT NULL,
    "search" TEXT,
    "create" TIMESTAMP(3) NOT NULL,
    "userId" INTEGER NOT NULL,
    "token" TEXT NOT NULL,

    CONSTRAINT "AppRefreshToken_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "MessageType" (
    "id" TEXT NOT NULL,
    "search" TEXT,
    "title" TEXT NOT NULL,
    "description" TEXT,

    CONSTRAINT "MessageType_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "AdmRefreshToken" (
    "id" SERIAL NOT NULL,
    "search" TEXT,
    "create" TIMESTAMP(3) NOT NULL,
    "managerId" INTEGER NOT NULL,
    "token" TEXT NOT NULL,

    CONSTRAINT "AdmRefreshToken_pkey" PRIMARY KEY ("id")
);

-- AddForeignKey
ALTER TABLE "AppRefreshToken" ADD CONSTRAINT "AppRefreshToken_userId_fkey" FOREIGN KEY ("userId") REFERENCES "User"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "MessageTemplate" ADD CONSTRAINT "MessageTemplate_messageTypeId_fkey" FOREIGN KEY ("messageTypeId") REFERENCES "MessageType"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "AdmRefreshToken" ADD CONSTRAINT "AdmRefreshToken_managerId_fkey" FOREIGN KEY ("managerId") REFERENCES "Manager"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
