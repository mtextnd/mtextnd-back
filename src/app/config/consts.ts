export const APP_REFRESH_TOKEN_LIVES = 14 * 24 * 60 * 60 * 1000; // 14 days in milliseconds

export const APP_TOKEN_EXPIRES_IN = '20m';
