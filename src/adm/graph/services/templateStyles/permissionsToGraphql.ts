import templateStylesBasePermissionToGraphql from './basePermissionsToGraphql';
import {TemplateStylesService} from '../../../services/TemplateStylesService/TemplateStylesService';
import {PermissionToGraphql} from '../../permissionsToGraphql';

// DO NOT EDIT! THIS IS GENERATED FILE

const templateStylesPermissionToGraphql: Partial<PermissionToGraphql<TemplateStylesService>> = {
  ...templateStylesBasePermissionToGraphql,
};

export default templateStylesPermissionToGraphql;
