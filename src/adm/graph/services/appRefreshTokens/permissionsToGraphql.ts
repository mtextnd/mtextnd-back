import appRefreshTokensBasePermissionToGraphql from './basePermissionsToGraphql';
import {AppRefreshTokensService} from '../../../services/AppRefreshTokensService/AppRefreshTokensService';
import {PermissionToGraphql} from '../../permissionsToGraphql';

// DO NOT EDIT! THIS IS GENERATED FILE

const appRefreshTokensPermissionToGraphql: Partial<PermissionToGraphql<AppRefreshTokensService>> = {
  ...appRefreshTokensBasePermissionToGraphql,
};

export default appRefreshTokensPermissionToGraphql;
