import permissionsBasePermissionToGraphql from './basePermissionsToGraphql';
import {PermissionsService} from '../../../services/PermissionsService/PermissionsService';
import {PermissionToGraphql} from '../../permissionsToGraphql';

// DO NOT EDIT! THIS IS GENERATED FILE

const permissionsPermissionToGraphql: Partial<PermissionToGraphql<PermissionsService>> = {
  ...permissionsBasePermissionToGraphql,
};

export default permissionsPermissionToGraphql;
