import messageTemplateLangVariantsBasePermissionToGraphql from './basePermissionsToGraphql';
import {MessageTemplateLangVariantsService} from '../../../services/MessageTemplateLangVariantsService/MessageTemplateLangVariantsService';
import {PermissionToGraphql} from '../../permissionsToGraphql';

// DO NOT EDIT! THIS IS GENERATED FILE

const messageTemplateLangVariantsPermissionToGraphql: Partial<PermissionToGraphql<MessageTemplateLangVariantsService>> = {
  ...messageTemplateLangVariantsBasePermissionToGraphql,
};

export default messageTemplateLangVariantsPermissionToGraphql;
