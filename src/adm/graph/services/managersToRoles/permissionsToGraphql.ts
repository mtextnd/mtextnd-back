import managersToRolesBasePermissionToGraphql from './basePermissionsToGraphql';
import {ManagersToRolesService} from '../../../services/ManagersToRolesService/ManagersToRolesService';
import {PermissionToGraphql} from '../../permissionsToGraphql';

// DO NOT EDIT! THIS IS GENERATED FILE

const managersToRolesPermissionToGraphql: Partial<PermissionToGraphql<ManagersToRolesService>> = {
  ...managersToRolesBasePermissionToGraphql,
};

export default managersToRolesPermissionToGraphql;
