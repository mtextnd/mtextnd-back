/* eslint-disable @typescript-eslint/no-empty-function */
import {
  TemplateStyle,
} from '../../../../generated/graphql';
import {Context} from '../../types';

export const afterUpdate = async (
  _ctx: Context,
  _data: TemplateStyle,
): Promise<void> => {};
