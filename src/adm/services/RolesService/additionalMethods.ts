import {Context} from '../types';
import {BaseRolesMethods} from './RolesService';

export interface AdditionalRolesMethods {}

export const getAdditionalMethods = (_ctx: Context, _baseMethods: BaseRolesMethods): AdditionalRolesMethods => ({});
