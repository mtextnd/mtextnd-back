import {Context} from '../types';
import {BaseLanguagesMethods} from './LanguagesService';

export interface AdditionalLanguagesMethods {}

export const getAdditionalMethods = (_ctx: Context, _baseMethods: BaseLanguagesMethods): AdditionalLanguagesMethods => ({});
