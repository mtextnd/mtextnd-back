/* eslint-disable @typescript-eslint/no-empty-function */
import {
  MailingType,
} from '../../../../generated/graphql';
import {Context} from '../../types';

export const afterUpdate = async (
  _ctx: Context,
  _data: MailingType,
): Promise<void> => {};
