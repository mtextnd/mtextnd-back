import {Context} from '../types';
import {BaseMailingTypesMethods} from './MailingTypesService';

export interface AdditionalMailingTypesMethods {}

export const getAdditionalMethods = (_ctx: Context, _baseMethods: BaseMailingTypesMethods): AdditionalMailingTypesMethods => ({});
