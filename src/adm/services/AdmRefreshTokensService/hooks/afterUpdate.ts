/* eslint-disable @typescript-eslint/no-empty-function */
import {
  AdmRefreshToken,
} from '../../../../generated/graphql';
import {Context} from '../../types';

export const afterUpdate = async (
  _ctx: Context,
  _data: AdmRefreshToken,
): Promise<void> => {};
