import {
  MutationUpdateAggregateTrackingArgs,
} from '../../../../generated/graphql';
import {Context} from '../../types';

export const additionalOperationsOnUpdate = async (
  _ctx: Context,
  _data: MutationUpdateAggregateTrackingArgs,
) => [];
