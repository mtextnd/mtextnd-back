import {
  MutationRemoveRolesToPermissionArgs,
} from '../../../../generated/graphql';
import {Context} from '../../types';

export const beforeDelete = async (
  _ctx: Context,
  _params: MutationRemoveRolesToPermissionArgs,
// eslint-disable-next-line @typescript-eslint/no-empty-function
): Promise<void> => {};
