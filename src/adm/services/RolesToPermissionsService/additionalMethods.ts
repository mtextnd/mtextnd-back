import {Context} from '../types';
import {BaseRolesToPermissionsMethods} from './RolesToPermissionsService';

export interface AdditionalRolesToPermissionsMethods {}

export const getAdditionalMethods = (_ctx: Context, _baseMethods: BaseRolesToPermissionsMethods): AdditionalRolesToPermissionsMethods => ({});
