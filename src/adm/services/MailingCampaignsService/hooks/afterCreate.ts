/* eslint-disable @typescript-eslint/no-empty-function */
import {
  MailingCampaign,
} from '../../../../generated/graphql';
import {Context} from '../../types';

export const afterCreate = async (
  _ctx: Context,
  _data: MailingCampaign,
): Promise<void> => {};
