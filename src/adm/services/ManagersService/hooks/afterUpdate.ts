/* eslint-disable @typescript-eslint/no-empty-function */
import {
  Manager,
} from '../../../../generated/graphql';
import {Context} from '../../types';

export const afterUpdate = async (
  _ctx: Context,
  _data: Manager,
): Promise<void> => {};
