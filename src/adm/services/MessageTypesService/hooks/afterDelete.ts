/* eslint-disable @typescript-eslint/no-empty-function */
import {
  MessageType,
} from '../../../../generated/graphql';
import {Context} from '../../types';

export const afterDelete = async (
  _ctx: Context,
  _data: MessageType,
): Promise<void> => {};
