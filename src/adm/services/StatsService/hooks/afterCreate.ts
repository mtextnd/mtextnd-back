/* eslint-disable @typescript-eslint/no-empty-function */
import {
  Stat,
} from '../../../../generated/graphql';
import {Context} from '../../types';

export const afterCreate = async (
  _ctx: Context,
  _data: Stat,
): Promise<void> => {};
