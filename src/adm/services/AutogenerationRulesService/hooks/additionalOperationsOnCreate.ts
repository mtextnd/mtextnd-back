import {
  MutationCreateAutogenerationRuleArgs,
} from '../../../../generated/graphql';
import {Context} from '../../types';

export const additionalOperationsOnCreate = async (
  _ctx: Context,
  _data: MutationCreateAutogenerationRuleArgs,
) => [];
