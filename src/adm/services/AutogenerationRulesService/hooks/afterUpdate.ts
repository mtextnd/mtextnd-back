/* eslint-disable @typescript-eslint/no-empty-function */
import {
  AutogenerationRule,
} from '../../../../generated/graphql';
import {Context} from '../../types';

export const afterUpdate = async (
  _ctx: Context,
  _data: AutogenerationRule,
): Promise<void> => {};
