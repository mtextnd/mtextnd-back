import {Context} from '../types';
import {BaseAutogenerationRulesMethods} from './AutogenerationRulesService';

export interface AdditionalAutogenerationRulesMethods {}

export const getAdditionalMethods = (_ctx: Context, _baseMethods: BaseAutogenerationRulesMethods): AdditionalAutogenerationRulesMethods => ({});
