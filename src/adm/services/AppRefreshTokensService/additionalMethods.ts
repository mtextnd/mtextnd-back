import {Context} from '../types';
import {BaseAppRefreshTokensMethods} from './AppRefreshTokensService';

export interface AdditionalAppRefreshTokensMethods {}

export const getAdditionalMethods = (_ctx: Context, _baseMethods: BaseAppRefreshTokensMethods): AdditionalAppRefreshTokensMethods => ({});
