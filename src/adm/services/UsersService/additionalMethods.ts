import {Context} from '../types';
import {BaseUsersMethods} from './UsersService';

export interface AdditionalUsersMethods {}

export const getAdditionalMethods = (_ctx: Context, _baseMethods: BaseUsersMethods): AdditionalUsersMethods => ({});
