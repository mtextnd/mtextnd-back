enum AppErrorCode {
  Unauthenticated = 'unauthenticated',
  BadCardNumber = 'badCardNumber',
  PasswordsDoNotMatch = 'passwordsDoNotMatch',
  ErrorOccurred = 'errorOccurred',
}

export default AppErrorCode;
